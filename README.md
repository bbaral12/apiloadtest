# APILoadTest

Introduction

	The test runs a load test with basic http requests against the api https://jsonplaceholder.typicode.com/.

Given

	users = 1, run time = 15 s

How to run the tests:

	API Automation  Test

		-Run with artifacts from gitlab
			--Clone the project
			--Run the following command from the shell
				npm run loadtest
				Note: Test is setup for 1 user to run for 15 seconds. Max iterations is set at 5.

		-Run with a docker image
			--Pull the docker image 
			--Pull the docker image bbaral12/apiload
			--Run the following command
				docker run bbaral12/apiload run -u 1 --duration 15s PerfTestAutomation.js	