//Only basic HTTP message assertion used to reduce client-side resource use for performance tests
import http from 'k6/http';
import { check, sleep } from 'k6';

const SLEEP_DURATION = 3;

export default function() {

    //declare headers here
    let params = {
        headers: {
            'Content-Type': 'application/json',
        }
    };

    let BASE_URL = 'https://jsonplaceholder.typicode.com';

    //Make a comment by User (userid = 3) on a post (id = 55) with their company catchphrase
    var commentuserid = 3;
    var postid = 55;

    //Get User response
    let get_user_response_1 = http.get(
        BASE_URL + '/users/' + commentuserid
    );
    
    check(get_user_response_1, {
        'Got user information': r => r.status === 200,
    });

    sleep(SLEEP_DURATION);

    var get_user_response_1_json = JSON.parse(get_user_response_1.body);

    let commentbody_1 = {
        'postId': postid,
        'name': 'test name',
        'email': get_user_response_1_json.email,
        'body': get_user_response_1_json.company.catchPhrase
    };

    //Make a comment on the post, pass email and catchphrase
    let get_user_response_2 = http.post(
        BASE_URL + '/comments',
        params,
        commentbody_1,
    );

    check(get_user_response_2, {
        'Comment is submitted': r => r.status === 201,
    });

    sleep(SLEEP_DURATION);

    //Edit a comment with id 10. Since no permissions are setup for comments, any user can edit any comments
    var editcommentid = 10;

    let editcommentbody_1 = {
        'email': get_user_response_1_json.email,
        'body': get_user_response_1_json.company.catchPhrase
    };

    //Edit the comment, pass email and catchphrase
    let get_user_response_3 = http.put(
        BASE_URL + '/comments/' + editcommentid,
        params,
        editcommentbody_1,
    );

    check(get_user_response_3, {
        'Comment is edited': r => r.status === 200,
    });

    sleep(SLEEP_DURATION);

    //Delete the comment with commentid 20
    var delcommentid = 20;
    let get_user_response_4 = http.del(
        BASE_URL + '/comments/' + delcommentid,
    );

    check(get_user_response_4, {
        'Comment is deleted': r => r.status === 200,
    });

    sleep(SLEEP_DURATION);
}